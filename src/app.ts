import {bootstrap} from '@angular/platform-browser-dynamic';
import {Clinic} from './app/app.component';
import {HTTP_PROVIDERS} from '@angular/http';
import {APP_ROUTER_PROVIDERS} from './app/app.routes';

bootstrap(Clinic, [
  APP_ROUTER_PROVIDERS,
  HTTP_PROVIDERS
]).catch(err => console.error(err));
