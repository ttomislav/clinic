import {Login} from './login/login.component';
import {Registration} from './registration/registration.component';
import {Access} from './access.component';
import {AuthService} from '../commons/auth.service';
import {AuthGuard} from '../commons/auth.guard';
import {RouterConfig} from '@angular/router';

export const AUTH_PROVIDERS = [AuthGuard, AuthService]

export const AccessRoutes: RouterConfig = [{
  path: '',
  redirectTo: '/access/registration',
  terminal: true
}, {
  path: 'access',
  component: Access,
  children: [{
    path: 'login',
    component: Login
  }, {
    path: 'registration',
    component: Registration
  }]
}]