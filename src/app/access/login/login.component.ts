import {Component} from '@angular/core';
import {NgForm} from '@angular/common';
import {AuthService} from '../../commons/auth.service';
import {User} from '../../commons/user';
import {Router} from '@angular/router';

@Component({
  selector: 'login',
  templateUrl: 'app/access/login/login.component.html'
})

export class Login {
  user = {};
  constructor(
    private authService: AuthService,
    private router: Router
  ){}

  onSubmit() {
    this.authService.loginUser().subscribe(() => {
      if (this.authService.loggedIn) {
        this.router.navigate(['/home']);
      }
    });
  }
}