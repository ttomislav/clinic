import {Component} from '@angular/core';
import {FormBuilder, Control, ControlGroup, Validators, FORM_DIRECTIVES} from '@angular/common'
import {Router} from '@angular/router';
import {AuthService} from '../../commons/auth.service';
import {EmailValidator} from '../../commons/email.validator';
import {IsSameValidator} from '../../commons/is-same.validator.ts';

@Component({
  selector: "registration",
  templateUrl: 'app/access/registration/registration.component.html',
  directives: [FORM_DIRECTIVES]
})

export class Registration{
   private newUser = {}

   form: ControlGroup;
   password: Control;
   confirmPassword: Control;
   first_name: Control;
   email: Control;
   last_name: Control;
   city: Control;
   phone: Control;

   constructor(
     private builder: FormBuilder,
     private authService: AuthService
   ) {
    this.first_name = new Control('', Validators.required)
    this.city = new Control('', Validators.required)
    this.phone = new Control('', Validators.required)
    this.first_name = new Control('', Validators.required)
    this.last_name = new Control('', Validators.required)

    this.email = new Control('', Validators.compose([
      Validators.pattern(EmailValidator.pattern),
      Validators.required
    ]), EmailValidator.taken)

    this.password = new Control('', Validators.compose([
      Validators.minLength(6),
      Validators.required
    ]));

    this.confirmPassword = new Control('', Validators.compose([
      Validators.required,
      IsSameValidator.check(this.password)
    ]))

    this.form = builder.group({
      last_name: this.last_name,
      first_name: this.first_name,
      email: this.email,
      password: this.password,
      confirmPassword: this.confirmPassword,
      phone: this.phone,
      city: this.city
    });
   }

   onSubmit() {
     console.log(this.newUser)
   }

}