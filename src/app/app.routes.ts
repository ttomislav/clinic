import {provideRouter, RouterConfig}  from '@angular/router';
import {AccessRoutes} from './access/access.routes';
import {HomeRoutes} from './home/home.routes';
import {AUTH_PROVIDERS} from './access/access.routes';

export const routes: RouterConfig = [
  ...AccessRoutes,
  ...HomeRoutes
];

export const APP_ROUTER_PROVIDERS = [
  provideRouter(routes),
  AUTH_PROVIDERS
];