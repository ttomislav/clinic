import {Control} from '@angular/common';

export class EmailValidator {
  static pattern =  '^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)' +
                    '*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*' +
                    '\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|' +
                    'net|org|pro|travel|mobi|[a-z][a-z])|' +
                    '([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$';

  static taken(control: Control) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (control.value === 'david@email.com') {
          resolve({ 'taken': true });
        } else {
          resolve(null);
        }
      }, 1000)
    });
  }

}