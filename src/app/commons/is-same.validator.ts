import {Control} from '@angular/common';

export class IsSameValidator {
  static check(checkedControl: Control) {
    return function checker(control: Control) {
      if(control.value !== checkedControl.value){
        return { 'isSame': true }
      }
      return null
    }
  }
}