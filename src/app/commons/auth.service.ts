import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import {User} from './user';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';

@Injectable()
export class AuthService {
  loggedIn: boolean = false

  constructor(private http: Http){}

  loginUser(){
    return Observable.of(true).delay(1000).do(val => this.loggedIn = true)
  }

  logout(){
    this.loggedIn = false
  }
}

