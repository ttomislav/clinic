import {Home} from './home.component';
import {AuthGuard} from '../commons/auth.guard';
import {RouterConfig} from '@angular/router';

export const HomeRoutes: RouterConfig = [{
  path: 'home',
  component: Home,
  canActivate: [AuthGuard]
}]