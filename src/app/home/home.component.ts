import {Component} from '@angular/core';
import {Http} from '@angular/http';
import {AuthService} from '../commons/auth.service'
import {Router} from '@angular/router';

@Component({
  selector: 'home',
  templateUrl: 'app/home/home.component.html'
})

export class Home {
  constructor(
    private http: Http,
    private authService: AuthService,
    private router: Router
  ) {}
  logout(){
    console.log('logout')
    this.authService.logout()
    this.router.navigate(['/access/login'])
  }
}