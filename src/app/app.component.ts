import {Component} from '@angular/core';
import {ROUTER_DIRECTIVES} from '@angular/router';

import {Registration} from './access/registration/registration.component';
import {Login} from './access/login/login.component';
import {Home} from './home/home.component';

@Component({
  selector: 'clinic',
  templateUrl: 'app/app.component.html',
  directives: [ROUTER_DIRECTIVES]
})

export class Clinic {}